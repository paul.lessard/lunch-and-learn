#!/bin/bash

function usage {
	echo "$0 start-services|stop-services|restart-services"
}

function start-services {
	echo "Starting services..."
	supervisord -c /etc/supervisord.conf >/tmp/supervisord.log &

}

function stop-services {
	echo "Stopping services..."
	pkill supervisord
}

function restart-services {
	stop-services
	sleep 3
	start-services
}

if [ $# -gt 0 ];
then
	case $1 in
		start-services)
			start-services
			;;
		stop-services)
			stop-services
			;;
		restart-services)
			restart-services
			;;
		go)
			start-services
			;;
		*)
			;;
	esac
fi

exit 0
