import requests
import json

data = {
        "crust" : "thin"
        ,"size": "medium"
        ,"toppings": ["cheese", "mushrooms"]
}

host=input("What is the server address? ")


#Our dictionary is posted as a www form data, but our "toppings" array gets clobbered!
print("Testing a post using params... this should fail.")
r = requests.post(f'http://{host}/api/v1/order_pizza', params=data)
print(f"{r.text}\n")

print("Testing a post using the json parameter... this should succeed.")
r = requests.post(f'http://{host}/api/v1/order_pizza', json=data)
print(f"{r.text}\n")

print("Testing a post using the data parameter, but we're dumping the JSON data to a string... this should succeed.")
r = requests.post(f'http://{host}/api/v1/order_pizza', data=json.dumps(data))
print(f"{r.text}\n")

print("Testing a post using the json parameter, but we're choosing and invalid topping... this should fail.")
data["toppings"].append("hot sauce")
r = requests.post(f'http://{host}/api/v1/order_pizza', data=json.dumps(data))
print(f"{r.text}\n")
