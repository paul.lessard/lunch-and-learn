import requests
import json
import argparse

parser = argparse.ArgumentParser(description='Order a pizza!')
parser.add_argument('-c', '--customer', help='Specifies a customer, for leaderboard glory! :D')
parser.add_argument('-t', '--toppings', help='Specify a comman delimited list of toppings.  Please enclose this argument in quotes.')
parser.add_argument('host', help='Specifies the host to connect to.  Should be an ip address or domain name.')

args = parser.parse_args()

data = {
        "crust" : "thin"
        ,"size": "medium"
        ,"toppings": ["cheese", "mushrooms"]
}

if args.toppings:
    data["toppings"] = list(map(lambda x: x.strip(), args.toppings.split(",")))

if args.customer:
    data["customer"] = args.customer

host=args.host


print("Ordering your pizza!")
r = requests.post(f'http://{host}/api/v1/order_pizza', json=data)
print(f"{r.text}\n")
