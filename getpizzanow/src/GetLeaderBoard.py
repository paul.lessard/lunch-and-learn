#!/usr/bin/env python
# WS client example
import asyncio, json, traceback
import websockets
from websockets.exceptions import ConnectionClosedError, ConnectionClosedOK
import argparse
import sys

DEBUG=False

parser = argparse.ArgumentParser(description='Order a pizza!')
parser.add_argument('uri', help='Specifies the host to connect to.  Should be a valid websocket URI, e.g. wss://host:port/endpoint or ws://host:port/endpoint.')

args = parser.parse_args()

async def getServerMessage(websocket):
    result = await websocket.recv()
    if DEBUG:
        print(f"Got a server message: {result}")
    return result
        
async def connect(uri):
    async with websockets.connect(uri) as websocket:
        registered = False
        server_msg_task = None
        while True:
            if not registered:
                asyncio.create_task(websocket.send("/leaderboard_register"))
            
            await asyncio.sleep(0)

            if server_msg_task is None: 
                server_msg_task = asyncio.create_task(getServerMessage(websocket))

            if server_msg_task is not None and server_msg_task.done():
                try:
                    msg = await server_msg_task
                    msg = json.loads(msg)

                    if (msg.get("cmd", None) == "reg" and msg.get("success", None) == True ):
                        registered = True
                        print("Successfully registered for the leaderboard!")
                    elif (msg.get("cmd", None) == "reg" and msg.get("success", None) == False ):
                        print(f"Failed to register for the leaderboard, trying again in a few moments...")
                        asyncio.sleep(0.5)
                    elif (msg.get("cmd", None) == "unreg"): 
                        registered = False
                        print(f"You have successfully unsubscribed.  Goodbye!")
                        sys.exit(0)
                    elif (msg.get("cmd", None) == "broadcast"):
                        ranks = msg.get("message", "No Leaderboard Update Message")
                        print(f"Latest leaderboard:\n{ranks}")
                except Exception as e:
                    print(f"Failed to handle a server message.  Exception was: {e}")
                    tb = traceback.format_exc()
                    print(f"Stack Trace:\n{tb}")
                finally:
                    server_msg_task = None


async def leaderboard():
    uri = args.uri
    while True:
        try:
            await connect(uri)
        except ConnectionClosedError as cce:
            print(f"Connection was lost... pausing before attempting to reconnect.")
            await asyncio.sleep(1)
        except OSError as e:
            print(f"Connection has failed, please try again later...")
            break

try:
    asyncio.get_event_loop().run_until_complete(leaderboard())
except KeyboardInterrupt as ki:
    print(f"Exiting.")
