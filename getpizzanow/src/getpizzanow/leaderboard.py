#!/usr/bin/env python
import asyncio, json
import websockets
from websockets.exceptions import ConnectionClosedError, ConnectionClosedOK
from getpizzanow.utils.redisutils import RedisUtils
from authlib.jose import jwt
import logging

FORMAT = '%(asctime)-15s %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger("getpizzanow.leaderboard")
log.setLevel(logging.DEBUG)

log.info(f"Starting up...")
def createJWT():
    header = {'alg': 'RS256'}
    payload = {'iss': 'Authlib', 'sub': '123', 'secret': 'bob'}
    s = jwt.encode(header, payload, key)
    return s


class Leaderboard:
    def __init__(self, outboxes):
        self.redis = RedisUtils()
        self.outboxes = outboxes
        self.scores = None


    async def waitThenCheck(self):
        await asyncio.sleep(1)
        await self.checkLeaderboard()

    async def checkLeaderboard(self):
        try:
            clientCount = await self.outboxes.clientCount()
            if clientCount > 0:
                log.debug(f"----Processing Leaderboard----")
                r = self.redis.conn()
                await asyncio.sleep(0)
                scores = []
                for score in r.zrevrangebyscore("leaderboard", '+inf', '-inf', withscores=True ):

                    (name, score) = score
                    scores.append({ "name": name.decode(), "score": float(score) })

                log.info(f"Latest scores: {json.dumps(scores)}")
                self.scores = scores
                await self.outboxes.broadcast(json.dumps(self.scores))
            else:
                print(f"No clients currently connecting.  Skipping leaderboard update.")
            
        except Exception as e:
            log.warning(f"Failed to check the leaderboard.  Exception was: {e}")
        finally:
            asyncio.create_task(self.waitThenCheck())

class Cmd:
    def __init__(self, cmd_type = None, args = None):
        try:
            self.cmd_type = cmd_type
            self.args = args
        except Exception as e:
            log.warning(f"Cannot initialize the Cmd.  Exception was: {e}")

class Outboxes:
    def __init__(self):
        try:
            self.clients = set()
            self.messages = asyncio.Queue()
        except Exception as e:
            log.warning(f"Failed to initialize the Outbox.  Exceptino was: {e}")

    async def clientCount(self):
        return len(self.clients)

    async def checkClient(self, websocket, path):
        try:
            self.clients.add(websocket)
        except Exception as e:
            log.warning(f"Failed to check/add client to clients list.  This client is probably broken.  Exception was: {e}")
            raise

    async def unregisterClient(self, websocket, path):
        try:
            self.clients.remove(websocket)
            log.info(f"Client removed from leaderboard broadcasts: {repr(websocket)}")
        except Exception as e:
            log.warning(f"Failed to unregister the client.  Exception was: {e}")
            raise


    async def sendNextMessage(self, websocket, msg):
        try:
            await websocket.send(msg)
        except ConnectionClosedOK as cco:
            #A client must have disconnected or gone stale...
            await self.unregisterClient(websocket, "*SendFailed*")
        except ConnectionClosedError as cce:
            #A client must have disconnected or gone stale...
            log.warning(f"Connection Closed.  WebSocket: {repr(websocket)}")
            await self.unregisterClient(websocket, "*SendFailed*")
        except Exception as e:
            log.warning(f"Failed to sendNextMessage.  Exception was: {e}")
            await self.unregisterClient(websocket, "*SendFailed*")
            raise

    async def broadcast(self, message):
        try:
            log.debug(f"Sending broadcast: {message}")
            clients = self.clients.copy()
            for ws in clients:
                msg = {
                        "cmd": "broadcast"
                        ,"type": "leaderboard_update"
                        ,"message": message
                }
                send_task = asyncio.create_task(self.sendNextMessage(ws, json.dumps(msg)))
        except Exception as e:
            log.warning(f"Failed to broadcast message.  Exception was: {e}")

async def handleRegistration(websocket, path):
    try:
        await outboxes.checkClient(websocket, path)
        msg = {
                "cmd": "reg"
                ,"success": True
                ,"message": "Registered to receive leaderboard updates!"
            }
        await websocket.send(json.dumps(msg))
    except Exception as e:
        log.warning(f"Failed to handleRegisrtation.  Dropping command.  Exception was: {e}")

async def handleUnSubscribe(websocket, path):
    try:
        await outboxes.unregisterClient(websocket, path)
        message = "You have been unsubscribed from leaderboard updates!"
        msg = {
                "cmd": "unreg"
                ,"success": True
                ,"message": message
                }
        await websocket.send(json.dumps(msg))
    except Exception as e:
        log.warning(f"Failed to handleUnSubscribe.  Exception was: {e}")
        raise

async def processMessage(websocket, path):
    global leaderboard_updating
    try:
        if not leaderboard_updating:
            log.debug(f"Starting leaderboard task")
            asyncio.create_task(leaderboard.checkLeaderboard())
            leaderboard_updating = True

        async for message in websocket:
            log.debug(f"Path: {path}; Got a message: {message}")
            
            #I wish I was on Python 3.10... matches are gonna rule!
            if  message == "/leaderboard_register":
                await handleRegistration(websocket, path) 
            elif message == "/leaderboard_unsubscribe":
                await handleUnSubscribe(websocket, path)
            else:
                await websocket.send("Sorry, I don't know that command!")
    except Exception as e:
        #Generally exceptions are due to user disconnects... either way, the 
        #connection appears to be unstable.
        log.info(f"A user has disconnected: {e}")
        await websocket.close()


leaderboard_updating = False
outboxes = Outboxes()
leaderboard = Leaderboard(outboxes)

start_server = websockets.unix_serve(processMessage, path="/run/ws_serve/leaderboard.sock")

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
