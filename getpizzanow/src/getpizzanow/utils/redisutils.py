import redis, json

class RedisUtils():
    def __init__(self):
        self._conn = redis.Redis(unix_socket_path="/run/redis/redis.sock")

    def init_redis(self):
        self._conn.set("company", "Get Pizza NOW!1!!ONE!!")
        self._conn.set("users", "{}")

        toppings = [
                "cheese"
                ,"mushrooms"
                ,"pepperoni"
                ,"peppers"
                ,"pinnaple"
                ,"ham"
                ,"black olives"
                ,"anchovies"
                ]
        self._conn.set("toppings", json.dumps(toppings))

        crusts = [
                "thin"
                ,"classic"
                ,"stuffed"
                ]
        self._conn.set("crusts", json.dumps(crusts))

        sizes = [
                "small"
                ,"medium"
                ,"large"
                ]
        self._conn.set("sizes", json.dumps(sizes))

        scores = {
                "cheese": 1
                ,"anchovies": 10
                ,"pepperoni": 2
                ,"black olives": 5
                ,"mushrooms": 2
                ,"pinnaple": -3
                }
        self._conn.set("scores", json.dumps(scores))

    def conn(self):
        return self._conn


