from flask import Flask, session, redirect, url_for, request, g, abort
import json
from enum import *
import os, sys, hashlib
from functools import wraps
import site
import logging, traceback
import time, datetime
import base64, hashlib
from getpizzanow.utils.redisutils import RedisUtils
import logging

FORMAT = '%(asctime)-15s %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger("getpizzanow.app")
log.setLevel(logging.DEBUG)

DEBUG=True

class StrEnum(str, Enum):
    pass


def create_app(env=None, start_response=None):

    # Begin App Creation
    app = Flask(__name__)

    # Set the secret key to some random bytes. Keep this really secret!
    app.secret_key = os.urandom(16)

    rds = RedisUtils()
    rds.init_redis()


    @app.before_request
    def before_req(*args, **kwargs):
        log.debug(f"--------------USER {request.remote_addr} is accessing {request.base_url}, using METHOD/VERB {request.method}-------------")

    @app.after_request
    def after_req(response, *args, **kwargs):
        log.info(f"Response back to user {request.remote_addr}: {response.get_data(as_text=True)}")
        log.debug(f"----------------END Request from USER {request.remote_addr}---------------------")
        return response

    def getToppings():
        toppings = None
        try:
            r = rds.conn()
            toppings = r.get("toppings")
            log.debug(f"Toppings: {toppings}")
            if toppings is not None:
                toppings = json.loads(toppings.decode())
        except Exception as e:
            log.warning(f"Failed to retrieve toppings list from REDIS.  Returning None.  Exception was: {e}")
            toppings = None
        return toppings

    def validateField(field, in_data):
        r = rds.conn()
        if isinstance(in_data, (list, dict)):
            options = r.get(field)
            if options is not None:
                options = json.loads(options.decode())
                for choice in in_data:
                    if choice not in options:
                        raise ValueError(f"Invalid choice '{choice}' for field {field}.")
            return True
        else:
            log.warning(f"Field {field} was not a dictionary or list as expected.  Please check your request and try again.")
            return False
        

    def validateToppings(in_data):
        return validateField("toppings", in_data)

    def validateSize(in_data):
        return validateField("sizes", [in_data])

    def validateCrust(in_data):
        return validateField("crusts", [in_data])


    def getPizzaScore(pizza):
        score = 0
        try:
            r = rds.conn()
            scores = json.loads(r.get("scores").decode())
            toppings = pizza.get("toppings", None)
            crust = pizza.get("crust", None)
            size = pizza.get("size", None)

            if toppings is not None:
                for topping in toppings:
                    score += scores.get(topping, 1)

        except Exception as e:
            log.warning(f"Failed to getPizzaScore.  Returning zero.  Exception was: {e}")
            score = 0
        return score

    def updateScore(customer_order):
        customer = customer_order.get("customer", None)
        try:
            if customer is None:
                raise ValueError(f"No customer supplied!  Cannot add score!")

            r = rds.conn()
            score = getPizzaScore(customer_order)
            
            r.zincrby("leaderboard", score, customer)
            log.info(f"Customer {customer} got this number of points for their order: {score}")
        except Exception as e:
            log.warning(f"Failed to add score for customer.  Exception was: {e}")


    def createOrder(data):
        result = None
        try:
            if "size" in data and "toppings" in data and "crust" in data:
                r = rds.conn()

                size = data["size"]
                log.debug(f"Size requested is: {size}")
                if not validateSize(size):
                    raise ValueError(f"Size was invalid.")

                toppings = data["toppings"]
                log.debug(f"Toppings requested are: {str(toppings)}")
                if not validateToppings(toppings):
                    raise ValueError(f"Toppings were invalid.")

                crust = data["crust"]
                log.debug(f"Crust requested is: {crust}")
                if not validateCrust(crust):
                    raise ValueError(f"Crust was invalid.")

                order_details = {
                            "size": size
                            ,"toppings": toppings
                            ,"crust": crust
                        }

                if "customer" in data:
                    order_details["customer"] = data["customer"]
                    updateScore(data)

                log.info(f"Order details is: {json.dumps(order_details)}")
                curr_order = int(r.incr("current_order"))
                r.set(f"order_number_{curr_order}", json.dumps(order_details))

                result = {"success": True, "order_number": curr_order}
            else:
                result = {
                        "success": False
                        ,"reason": "You must specify all of these parameters: size, toppings, crust"
                    }
        except ValueError as e:
            raise e
        except Exception as e:
            log.warning(f"Failed to create a new pizza order. Exception was: {e}")
            result = {
                    "success": False
                    ,"reason": "There was an error processing your order.  Please check your order for any errors, then resubmit."
                }
        return result

    def updateOrder(order_number, data):
        result = None
        try:
            log.debug(f"Updating order...")
            if "size" in data and "toppings" in data and "crust" in data:
                r = rds.conn()

                size = data["size"]
                log.debug(f"Size requested is: {size}")
                if not validateSize(size):
                    raise ValueError(f"Size was invalid.")

                toppings = data["toppings"]
                log.debug(f"Toppings requested are: {str(toppings)}")
                if not validateToppings(toppings):
                    raise ValueError(f"Toppings were invalid.")

                crust = data["crust"]
                log.debug(f"Crust requested is: {crust}")
                if not validateCrust(crust):
                    raise ValueError(f"Crust was invalid.")

                order_details = {
                            "size": size
                            ,"toppings": toppings
                            ,"crust": crust
                        }


                log.info(f"Update details is: {json.dumps(order_details)}")
                r.set(f"order_number_{order_number}", json.dumps(order_details))

                result = {"success": True, "order_number": order_number}
            else:
                result = {
                        "success": False
                        ,"reason": "You must specify all of these parameters: size, toppings, crust"
                    }
        except ValueError as e:
            raise e
        except Exception as e:
            log.warning(f"Failed to update pizza order. Exception was: {e}")
            result = {
                    "success": False
                    ,"reason": "There was an error processing your order.  Please check your order for any errors, then resubmit."
                }
        return result


    def checkOrder(order_number, data):
        result = None
        try:
            r = rds.conn()

            try:
                ordr = r.get(f"order_number_{order_number}")
                if ordr is not None:
                    ordr = json.loads(ordr.decode())
                    log.debug(f"Order data: {ordr}")
                    result = {"success": True, "order_details": ordr}
                else:
                    raise ValueError("Order Not Found.")
            except Exception as e:
                log.warning(f"Error checking order.  Exception was: {e}")
                result = {
                            "success": False
                            ,"reason": f"I'm sorry.  I couldn't find order number: {order_number}"
                        }
        except Exception as e:
            log.warning(f"Failed to checkOrder.  Exception was: {e}")
        return result


    ############### Flask App Routes ################


    @app.route('/', defaults={'u_path': ''})
    @app.route('/<path:u_path>')
    def catch_all(u_path):
        abort(404)

    @app.route(F'/api/v1/')
    def index():
        if 'username' in session:
            return 'Logged in as %s' % escape(session['username'])
        return 'You are not logged in'

    @app.route(F'/api/v1/logout')
    def logout():
        # remove the username from the session if it's there
        session.pop('user_id', None)
        return json.dumps({ "success": True })

    @app.route(F'/api/v1/order_pizza', methods=['POST', 'PATCH'])
    def order_pizza():
        result = None
        try:
            data = None
            if request.is_json:
                data = request.json
            else:
                data = request.get_data(as_text=True)
                try:
                    data = json.loads(data)
                except:
                    #data must not be json data, just accept as-is
                    pass

            log.debug(f"Input Args: {data}")

            if request.method == "POST":
                result = createOrder(data)
            elif request.method == "PATCH":
                order_number = data.get("order_number", None)
                if order_number is None:
                    raise ValueError(f"No order number was provided for updating.")
                result = updateOrder(order_number, data)

        except ValueError as e:
            result =  {
                    "success": False
                    ,"reason": f"{e}"
                    }
        except Exception as e:
            log.warning(F'Could not accept pizza order.  Exception was: {e}')

        return json.dumps(result)

    @app.route(F'/api/v1/update_order/<int:order_number>', methods=['PATCH'])
    def update_order(order_number):
        result = None
        try:
            data = None
            if request.is_json:
                data = request.json
            else:
                data = request.form
                try:
                    data = json.loads(data)
                except:
                    #data must not be json data, just accept as-is
                    pass

            log.debug(f"Input Args: {data}")
            result = updateOrder(order_number, data)

        except ValueError as e:
            result =  {
                    "success": False
                    ,"reason": f"{e}"
                    }
        except Exception as e:
            log.warning(F'Could not update pizza order.  Exception was: {e}')

        return json.dumps(result)

    @app.route(F'/api/v1/check_order/<int:order_number>', methods=['GET'])
    def check_order_rest(order_number):
        result = None
        try:
            data = request.args

            log.debug(f"Input arguments: {json.dumps(data)}")
            result = checkOrder(order_number, data)
        except Exception as e:
            log.warning(F'Failed to check order.  Exception was: {e}')
        return json.dumps(result)   


    @app.route(F'/api/v1/check_order', methods=['GET'])
    def check_order():
        result = None
        try:
            data = request.args

            log.debug(f"Input arguments: {json.dumps(data)}")
            if "order_number" in data:
                order_number = data["order_number"]
                result = checkOrder(order_number, data)
            else:
                result = {
                        "success": False
                        ,"reason": "You must specify all of these parameters: order_number"
                    }
        except Exception as e:
            log.warning(F'Failed to check order.  Exception was: {e}')
        return json.dumps(result)   

    @app.route(F'/api/v1/toppings', methods=['GET'])
    def get_toppings():
        result = None
        try:
            toppings = getToppings()
            if toppings is not None:
                result = {
                        "success": True
                        ,"toppings": toppings
                        }
        except Exception as e:
            log.warning(f"Failed to retrieve the list of toppings.  Exception was: {e}")
            result = {
                    "success": False
                    ,"reason": "There was an error processing your request."
                }
        return json.dumps(result)

    return app

app = None
if (__name__ == "__main__"):
    log.info("Starting in __main__ mode...")
    app = create_app()

if (__name__=="openpulseox_api.app"):
    log.info("Starting uwsgi mode...")
