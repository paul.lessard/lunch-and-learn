#!/usr/bin/env python

import asyncio, json
import websockets
from websockets.exceptions import ConnectionClosedError, ConnectionClosedOK
import logging
from authlib.jose import jwt

FORMAT = '%(asctime)-15s %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger("getpizzanow.echo")
log.setLevel(logging.DEBUG)

#This is the working "echo" websocket example server.
async def echo(websocket, path):
    try:
        host = websocket.request_headers.get_all("x-forwarded-for")
        log.warning(f"{json.dumps(host)}")
        async for message in websocket:
            log.info(f"Path: {path}; {host} sent message: {message}")

            header = {'alg': 'RS256'}
            payload = { 
                    "iss": "echoservice"
                    ,"encoded_jwt": f"Super Secret JWT Encoded Message: {message}"
                    }
            key = open('/etc/system_key.pem', 'r').read()
            jwt_text = jwt.encode(header, payload, key).decode()
            msg = {
                    "message": message
                    ,"jwt": jwt_text
            }
            await websocket.send(json.dumps(msg))
    except ConnectionClosedError as cce:
        await websocket.close()
        log.warning(f"It seems a client has disconnected.  Exception was: {cce}")
    except ConnectionClosedOK as cco:
        await websocket.close()
        log.warning(f"It seems a client has disconnected.  Exception was: {cco}")

start_server = websockets.unix_serve(echo, path="/run/ws_serve/echo.sock")

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
