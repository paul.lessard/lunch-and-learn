#!/usr/bin/env python
# WS client example
import asyncio, json, traceback
import websockets
from websockets.exceptions import ConnectionClosedError, ConnectionClosedOK
import argparse
import sys
from datetime import datetime

parser = argparse.ArgumentParser(description='Test and echo web socket.')
parser.add_argument('uri', help='Specifies the host to connect to.  Should be a valid websocket URI, e.g. wss://host:port/endpoint or ws://host:port/endpoint.')
parser.add_argument('-m','--message', help='The message to send to the echo websocket.  If not specified, the message will be "Hello World!"')

args = parser.parse_args()

DEBUG=False

msg = None
if args.message:
    msg = args.message
else:
    msg = "Hello World!"

async def hello():
    uri = args.uri
    async with websockets.connect(uri) as websocket:
        while True:
            try:
                time = datetime.utcnow()
                print(f"{time.isoformat()}: Sending message to server: {msg}")
                await websocket.send(msg)
                message = await websocket.recv()
                time = datetime.utcnow()
                print(f"{time.isoformat()}: Message received from server: {message}")
                await asyncio.sleep(0.5)
            except ConnectionClosedError as cce:
                await websocket.close()
                break
            except KeyboardInterrupt:
                await websocket.close()
                break
        print(f"{time.isoformat()}: Disconnected from server.")
        sys.exit(0)        

asyncio.get_event_loop().run_until_complete(hello())
asyncio.get_event_loop().run_forever()
