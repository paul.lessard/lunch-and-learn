import requests
import json

data_w_order = {
        "order_number": 1
        ,"crust" : "thin"
        ,"size": "medium"
        ,"toppings": ["cheese", "mushrooms", "hot sauce"]
}

data = {
        "crust" : "thin"
        ,"size": "medium"
        ,"toppings": ["cheese", "mushrooms", "pepperoni"]
}


host=input("What is ther server address? ")

r = requests.patch(f'http://{host}/api/v1/order_pizza', json=data_w_order)
print(r.text)

r = requests.patch(f'http://{host}/api/v1/update_order/1', json=data)
print(r.text)

#Warning!!! Update Order has a vulnerability in it!!! It will take ANY order number, not just valid ones!!!
r = requests.patch(f'http://{host}/api/v1/update_order/999999999', json=data)
print(r.text)
