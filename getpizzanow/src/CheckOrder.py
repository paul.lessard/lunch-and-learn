import requests
import json

data_w_order = {
        "order_number": 1
        ,"crust" : "thin"
        ,"size": "medium"
        ,"toppings": ["cheese", "mushrooms"]
}

data = {
        "crust" : "thin"
        ,"size": "medium"
        ,"toppings": ["cheese", "mushrooms"]
}


host=input("What is ther server address? ")

r = requests.get(f"http://{host}/api/v1/check_order", params=data_w_order)
print(f"{r.text}\n")

r = requests.get(f'http://{host}/api/v1/check_order/1', json=data)
print(f"{r.text}\n")
