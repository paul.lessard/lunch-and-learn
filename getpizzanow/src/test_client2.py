#!/usr/bin/python
from websocket import create_connection

ws = create_connection("ws://172.17.0.2/sock/")
print ("Sending 'Hello, World'...")
ws.send("Hello, World")
print ("Sent")
print ("Receiving...")
result =  ws.recv()
print ("Received '%s'" % result)
input("Waiting for enter key...")
ws.close()
