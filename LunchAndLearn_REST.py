import json

class MyClass:
    def __init__(self):
        self.myvar = True


MyDict={ "myvar": True }
my_class=MyClass()

print("Test")

if hasattr(my_class, "myvar"):
    print("Class has myvar attached as an attribute")
else:
    print("Class DOESN'T have myvar attached as an attribute")

if "myvar" in MyDict:
    print("Dictionary has myvar as a key")
else:
    print("Dictionary DOESN'T have myvar as a key")

