import json

class Vehicle:
    def __init__():
        print("This is the constructor for Vehicle")

class Car(Vehicle):
    def __init__():
        super().__init__()
        print("This is the constructor for Car")

class Animal(object):
    def __init__(self, animal_type):
        print('Animal Type:', animal_type)

    def speak(self, message):
        print('Base animals don\'t say nothin')
    
class Mammal(Animal):
    def __init__(self):
        # call superclass
        super().__init__('Mammal')
        print('Mammals give birth directly')

    def speak(self, msg):
        print(F"{msg}")

    def super_speak(self, msg):
        super().speak(msg)

dog = Mammal()

dog.speak("Bark")
dog.super_speak("Bark")
